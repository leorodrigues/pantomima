const SERVER_PORT = parseInt(process.env.PORT || 3000);

const express = require('express');

const { shutdownAndExit, saveSeed, monitorResources } = require('./system');
const { World, WorldRunner, lorenzUpdate } = require('./model');
const routes = require('./routes');

const SEED = require('./seed.json');

const world = new World({ }, lorenzUpdate(SEED));
const worldRunner = new WorldRunner(world, 100);
const shutdownProcedure = saveSeed(world);

const app = express();
routes.mount(app, world);

app.listen(SERVER_PORT, () => {
    monitorResources(1000, `Listening on port ${SERVER_PORT}.`);
});

worldRunner.run();

for (const signal of ['SIGINT', 'SIGTERM', 'SIGQUIT', 'SIGHUP'])
    process.on(signal, shutdownAndExit(0, shutdownProcedure));
