const fs = require('fs');

function saveSeed(world) {
    return () => fs.writeFileSync('seed.json', JSON.stringify(world.state.atmosfere));
}

module.exports = { saveSeed };