module.exports = {
    StatusBar: require('./status-bar'),
    ...require('./monitor-resources'),
    ...require('./signal-handling'),
    ...require('./save-seed')
}