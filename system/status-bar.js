module.exports = class StatusBar {
    constructor(stream = process.stdout) {
        Object.assign(this, { stream, y: stream.rows - 1 });
    }

    initialize(text) {
        this.stream.on('resize', () => {
            if (this.stream.rows > this.y + 1) {
                this.stream.cursorTo(0, this.y);
                this.stream.clearLine(0);
            }
            this.y = this.stream.rows - 1;
        });
        return this.update(text);
    }

    update(text) {
        this.stream.cursorTo(0, this.y);
        this.stream.clearLine(0);
        this.stream.write(` - ${text} `);
        return this;
    }
};