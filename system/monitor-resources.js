// const tty = require('tty');

// const ora = require('ora');

const StatusBar = require('./status-bar');

function monitorResources(interval, message) {
    const bar = new StatusBar().initialize(message);
    setInterval(() => {
        const cpu = process.cpuUsage().user;
        const heap = process.memoryUsage().heapUsed;
        bar.update(`CPU: ${cpu} H: ${heap}`);
    }, interval);
}

module.exports = { monitorResources };