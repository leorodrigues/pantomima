function shutdownAndExit(errorCode, shutdown) {
    return () =>  {
        try {
            shutdown();
        } finally {
            process.exit(errorCode);
        }
    };
}

module.exports = { shutdownAndExit }