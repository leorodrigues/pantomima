module.exports = class WorldRunner {
    constructor(world, tickInterval = 10) {
        Object.assign(this, { world, tickInterval });
    }

    async run() {
        const { world, tickInterval, timerId } = this;
        if (timerId) clearInterval(timerId);
        this.timerId = setInterval(() => world.update(), tickInterval);
    }

    async stop() {
        const { timerId } = this;
        if (timerId) clearInterval(timerId);
    }
};