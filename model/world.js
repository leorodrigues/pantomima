
module.exports = class World {
    constructor(context, ...updateProcedures) {
        Object.assign(this, { context, updateProcedures });
    }

    update() {
        for (const p of this.updateProcedures)
            p(this.context);
    }

    get state() {
        const { context } = this;
        return new Proxy(context, STATE_HANDLER);
    }
};

const STATE_HANDLER = {
    get(target, property) {
        return target[property];
    }
};