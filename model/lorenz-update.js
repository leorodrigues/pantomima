const { lorenz } = require('@leorodrigues/chaos');

const DELTA = 0.000001;

module.exports = function lorenzUpdate(seed = [1.0, 1.0, 1.0], tDelta) {
    seed = [...seed];
    const lorenzFn = lorenz({ timeDelta: tDelta || DELTA });
    return (ctx) => ctx.atmosfere = lorenzFn(ctx.atmosfere || seed);
};