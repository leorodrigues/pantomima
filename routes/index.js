const getAtmosfere = require('./get-atmosfere');

module.exports = {
    mount(server, world) {
        server.get('/atmosfere', getAtmosfere(world));
    }
};