module.exports = function getAtmosfere(world) {
    return (request, response) => response.json(world.state.atmosfere);
};